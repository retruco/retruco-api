# Retruco-API (DEPRECATED)

_HTTP API to bring out shared positions from argumented statements_

Retruco is a software fostering argumentative discussion around statements and allowing to bring out shared positions.

**_Retruco_** means _immediate, precise and firm response_ in spanish.

This repository is now **deprecated**. Development continues on [retruco](https://framagit.org/retruco/retruco) repository.

## Installation

For the first time only:

```bash
npm install
```

### Create the database

```bash
su - postgres
createuser -D -P -R -S retruco
  Enter password for new role: retruco
  Enter it again: retruco
createdb -E utf-8 -O retruco retruco
psql retruco
  CREATE EXTENSION IF NOT EXISTS pg_trgm;
  \q
```

### Configure the API server

```bash
npm run configure
```

### Launch the API server

```bash
npm run start
```

### Launch the daemon that handles pending actions

In another terminal:

```bash
node process-actions.js
```
